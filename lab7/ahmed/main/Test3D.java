package ahmed.main;
import java.util.ArrayList;

import ahmed.shapes3d.Cube;
import ahmed.shapes3d.Cylinder;
public class Test3D {
	public static void main(String[] args) {
		
		Cylinder cylinder_1=new Cylinder();
		Cylinder cylinder_2=new Cylinder();
		Cylinder cylinder_3=new Cylinder();
		
		//System.out.println(cylinder_1.area());
		//System.out.println(cylinder_1.volume());
		//System.out.println(cylinder_1.toString());
		//System.out.println();
		
		Cube cube_1=new Cube();
		Cube cube_2=new Cube();
		Cube cube_3=new Cube();
		
		//System.out.println(cube_1.area());
		//System.out.println(cube_1.volume());
		//System.out.println(cube_1.toString());
		
		ArrayList<Cylinder> cylinders=new ArrayList<Cylinder>();
		cylinders.add(cylinder_1);
		cylinders.add(cylinder_2);
		cylinders.add(cylinder_3);
		
		ArrayList<Cube> cubes=new ArrayList<Cube>();
		cubes.add(cube_1);
		cubes.add(cube_2);
		cubes.add(cube_3);
	}

}
