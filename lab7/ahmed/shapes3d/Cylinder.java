package ahmed.shapes3d;
import ahmed.shapes.Circle;

public class Cylinder extends Circle  {
int height;
	 public Cylinder(int i, int j) {
		 super.radius = i;
	     height       = j;
}
     public Cylinder() {
    	 super.radius = 5;
	     height       = 4;
	 }
     
	 @Override
	 public double area() {
		return 2* super.area() + 2 * Math.PI *radius * height ;
		 
	 }
	 public double volume() {
			return super.area()*height;
			 
		 }
	 
	 @Override
	 public String toString() {
         return "radius = " + radius + " ," + " height = " + height;		
		 
	 }
}
