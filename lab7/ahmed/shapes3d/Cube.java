package ahmed.shapes3d;

public class Cube extends Square {
     public Cube(int r) {
    	 super.rib=r;
     }
     
     public Cube() {
    	 super.rib=5;
     }
     
     @Override
     public int area() {
		return 6 * super.area();
     }
     
     public int volume() {
    	return super.area() * rib; 
     }
     
     @Override
	 public String toString() {
         return "Rib = " + rib + " ," + " Area = " + area() + " ," + " Volume = " + volume();		
		 
	 }
}
