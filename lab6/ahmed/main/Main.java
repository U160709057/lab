package ahmed.main;
import java.util.ArrayList;

import ahmed.shapes.Circle;
public class Main {

	public static void main(String[] args) {
		Circle circle_1=new Circle(3);
		Circle circle_2=new Circle(4);
		Circle circle_3=new Circle(5);

		ArrayList<Circle> circles=new ArrayList<>();
		circles.add(circle_1);
		circles.add(circle_2);
		circles.add(circle_3);
		
		for (int i=0 ; i<circles.size();i++) {
			System.out.println(circles.get(i).area());
		}
	}

}
