package ahmed.shapes;
public class Circle {
protected int radius;
 public  Circle(int radius) {
	 this.radius=radius;
 }
 
 public  Circle() {
	 this.radius=5;
 }
 
 
 public  double area() {
	 return Math.PI * radius * radius ;
 }
}
