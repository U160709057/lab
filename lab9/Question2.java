public class Question2{
     public static void main(String[] args){
          System.out.println(min(4,5,9));
     }
     private static int min(int a , int b , int c ){
         int minimum= a<b ? a:b;
         return minimum<c ? minimum:c;
     }
}
