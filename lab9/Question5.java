public class Question5{
    public static void main(String[] args){
        int[][] values= {{1,2,3},{5,6},{9,8,7}};
        System.out.println(calculateAverage(values));
    }
    private static double calculateAverage(int [][] values){
         double sum=0;
         int counter=0;
         for (int i=0; i < values.length ; i++){
             for(int j=0; j< values[i].length ; j++){
                 sum += values[i][j];
                 counter++;
             }
         }
         return sum/counter;
    }
}

