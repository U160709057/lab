package ahmed.shapes;
public class Box extends Rectangle {
int height;
     public  Box(int length, int width,int height) {
	 this.height=height;
         super(length,height);
     }

     public  double area() {
	 return 2 * super.area() + 2 * height * width + 2 * height * length ;
     }
     public int volume(){
         return super.area * 6 ;
}
