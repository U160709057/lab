package stack;
public class StackItem <T> {

	private T item;
	private StackItem<T> previous;
	public StackItem(T obj , StackItem<T> previous){
      	        this.item=obj;
      		this.previous=previous;
        }
        public T getItem() {
                return item;
        }
        public StackItem<T> getPrevious() {
                return previous;
        }
       
}



