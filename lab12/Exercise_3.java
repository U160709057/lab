
public class Exercise_3 {
	public static void main(String[] args) {
                System.out.println("BY using the max method without generics");
		System.out.printf("Maximum of %d, %d and %d is %d\n\n", 3, 4, 5, max(3, 4, 5));
		System.out.printf("Maximum of %.2f, %.2f and %.2f is %.2f\n\n", 6.6, 8.89, 7.7, max(6.67, 8.89, 7.75));
		System.out.printf("Maximum of %s, %s and %s is %s\n", "pear", "apple", "orange",
				max("pear", "apple", "orange"));
        System.out.println("By using the max method with generics");
		System.out.printf("Maximum of %d, %d and %d is %d\n\n", 3, 4, 5, max1(3, 4, 5));
		System.out.printf("Maximum of %.2f, %.2f and %.2f is %.2f\n\n", 6.6, 8.89, 7.7, max1(6.67, 8.89, 7.75));
		System.out.printf("Maximum of %s, %s and %s is %s\n", "pear", "apple", "orange",
				max1("pear", "apple", "orange"));
		
	}

	static Comparable max1(Comparable i, Comparable j, Comparable k) {
		Comparable val = i;
		if (j.compareTo(val) > 0) {
			val = j;
		}
		if (k.compareTo(val) > 0) {
			val = k;
		}
		return val;
	}
	//by_using_generics
	static <T extends Comparable<T>> T max(T i , T j , T k) {
		Comparable val = i;
		if (j.compareTo((T) val) > 0) {
			val = j;
		}
		if (k.compareTo((T) val) > 0) {
			val = k;
		}
		return (T) val;
	}
}
