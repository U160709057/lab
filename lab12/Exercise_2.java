
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercise_2 {
	public static void main(String[] args) {
		List<Number> numList = new ArrayList<>();
		List<Integer> intList = Arrays.asList(1, 2, 3, 4);
		List<Double> dList = Arrays.asList(1.3, 2.0, 3.5, 4.0);

		copy_Int_Collection(intList, numList);
		
		copy_D_Collection(dList, numList);
		
		copyCollection(dList, numList);
		System.out.println(numList);
	}
    
	private static void copy_Int_Collection(List<Integer> intList, List<Number> numList) {
		for (int i : intList) {
			numList.add(i);
		}
	}

	private static void copy_D_Collection(List<Double> dList, List<Number> numList) {
		for (Double i : dList) {
			numList.add(i);
		}

	}
	
	
	//By_using_generics
	private static <T> void copyCollection(List <? extends T> dList,List <T> numList) {
		for (T i : dList) {
			numList.add(i);
		}
			
	}
}
