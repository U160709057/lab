
import java.util.ArrayList;
import java.util.List;

public class Exercise_1 {

	public static void main(String[] args) {
		String[] strs = { "a", "b", "c", "d" };
		List<String> strList = new ArrayList<>();
		addToCollection(strs, strList);
		System.out.println(strList);

		Integer[] ints = { 1, 2, 3, 4 };
		List<Integer> intList = new ArrayList<>();
		addToCollection(ints, intList);
		System.out.println(intList);

		// Exercise_1.addToCollection(ints, intList);
		// Exercise_1.addToCollection(strs, strList);

	}

	public static void addToCollection(Integer[] ints, List<Integer> intList) {
		System.out.println("The Int method was used");
		for (Integer int1 : ints) {
			intList.add(int1);
		}

	}

	public static void addToCollection(String[] strs, List<String> strList) {
		System.out.println("The Str method was used");
		for (String str : strs) {
			strList.add(str);
		}

	}

	// By_using_Generics
	public static <T> void addToCollection(T[] arr, List<T> list) {
		System.out.println("The generics method was used");
		for (T val : arr) {
			list.add(val);
		}

	}

}
