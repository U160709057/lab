public class Main{
 
   public static void main (String[] args){
        //  test Rectangle class //     
   	Rectangle rectA = new Rectangle();
   	rectA.sideA = 5;
   	rectA.sideB = 6;
        System.out.println(rectA.area());
        System.out.println(rectA.perimeter());
         
        //  test Circle class // 
        Circle circle_A = new Circle();
        circle_A.radius = 10;
        System.out.println(circle_A.area());
        System.out.println(circle_A.perimeter());

  }
}
