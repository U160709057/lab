package drawingV3;

import java.util.ArrayList;
import drawingV1.Rectangle;

public class Drawing {
     ArrayList<Shape> shapes = new ArrayList<Shape>();
     public double calculateTotalArea(){ 
     double totalArea = 0;
     for (Shape shape : shapes){
    	  totalArea += shape.area(); 
    	  } 
     return totalArea;
     }
     public void addShape(Shape obj) {
 		shapes.add(obj);
 	}
	
}
