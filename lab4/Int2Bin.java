public class Int2Bin{
      public static void main(String[] args){
           int decimal=Integer.parseInt(args[0]);
           if (decimal >=0){
               System.out.println(Int2BinRec(decimal));
           }
           else{
               System.out.println("Invalid number");
           }
               
      }

      private static String Int2BinRec(int num){
             if (num ==0 || num == 1){ 
                return num + "";
             }               
             else{
                return Int2BinRec(num/2) + num % 2 ;
             }
      }
}
