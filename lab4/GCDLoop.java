public class GCDLoop{
     public static void main(String[] args){
         int first= Integer.parseInt(args[0]);
         int second= Integer.parseInt(args[1]);
         int remainder=0;
     
         if (second>first){
            remainder=first;
            first=second;
            second=remainder;
         }
         while(second !=0){
            remainder=second;
            second = first % second;
            first=remainder; 
            if (second==0){
               System.out.println(first);
            }
         }  
                   
 
     }
}      
