public class GCDRec{
      public static void main(String[] args){
             int first= Integer.parseInt(args[0]);
             int second= Integer.parseInt(args[1]);
             int remainder;
     
             if (second>first){
            	 remainder = first;
           	 first = second;
           	 second = remainder;
             }
             int gcd = gcd(first,second);
             System.out.println(gcd);
      }        
      public static int gcd (int first,int second) {
         if (second !=0){
             return gcd(second, first % second);
         }
         else{
             return first;
         }
      }
 }   
